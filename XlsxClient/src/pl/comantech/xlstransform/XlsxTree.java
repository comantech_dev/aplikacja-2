/*
 * Aplikacja wyświetla strukturę zbudowaną w zadaniu 1 w formie rozwijalnego/zwijalnego drzewka.
 */
package pl.comantech.xlstransform;

import java.awt.BorderLayout;
import java.net.MalformedURLException;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

/**
 *
 * @author Sebastian
 */
public class XlsxTree extends JFrame {

    JTree tree;
    JScrollPane scrollPane = new JScrollPane();

    public static void main(String[] args) {
        try {
            XlsxTree app = new XlsxTree();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public XlsxTree() throws Exception {

        tree = new JTree(getTreeNode());

        getContentPane().setLayout(new BorderLayout());
        scrollPane.getViewport().add(tree);

        getContentPane().add("Center", scrollPane);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setSize(500, 500);
        setVisible(true);
    }

    private DefaultMutableTreeNode getTreeNode() throws NotBoundException, MalformedURLException, RemoteException {

        RmiServer rmiServer = (RmiServer) Naming.lookup("//localhost/TransformServer");
        List<Node> nodes = rmiServer.getNodes();

        DefaultMutableTreeNode treeRoot = new DefaultMutableTreeNode();

        for (Node node : nodes) {

            DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(node);

            for (Node child : node.getNodes()) {
                treeNode.add(addChild(child));
            }
            treeRoot.add(treeNode);
        }
        return treeRoot;
    }

    private DefaultMutableTreeNode addChild(Node child) {

        DefaultMutableTreeNode treeChild = new DefaultMutableTreeNode(child);

        for (Node next : child.getNodes()) {

            DefaultMutableTreeNode treeNext = new DefaultMutableTreeNode(next);

            if (!next.getNodes().isEmpty()) {
                treeNext.add(addChild(next));
            }

            treeChild.add(treeNext);
        }
        return treeChild;
    }
}
